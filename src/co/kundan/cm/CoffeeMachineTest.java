package co.kundan.cm;

import co.kundan.cm.request.Request;
import com.google.gson.Gson;
import org.json.simple.parser.JSONParser;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoffeeMachineTest {

    private static CoffeeMachineImpl coffeeMachineImpl;
    private static List<Drink> drinks = new ArrayList<>();
    private static List<Ingredient> ingredients = new ArrayList<>();
    private static Map<Ingredient, Integer> ingredientStock = new HashMap<>();
    private static Request request = new Request();

    @BeforeClass
    public static void setUp(){
        JSONParser parser = new JSONParser();

        try {
            Object obj = parser.parse(new FileReader("testData.json"));
            request = new Gson().fromJson(String.valueOf(obj), Request.class);
        } catch (Exception e){
            System.out.println();
        }


        for (Map.Entry<String, Integer> map : request.getMachine().getTotalItemsQuantity().entrySet()){
            Ingredient ingredient = new Ingredient(map.getKey());
            ingredients.add(ingredient);
            ingredientStock.put(ingredient, map.getValue());
        }


        for (Map.Entry<String, Map<String, Integer>> beverages : request.getMachine().getBeverages().entrySet()){
            Map<Ingredient, Integer> ingredientsRequired = new HashMap<>();
            for (Map.Entry<String, Integer> map : beverages.getValue().entrySet()){
                Ingredient ingredient = ingredients.stream().filter(x -> map.getKey().equals(x.getName()))
                        .findAny().orElse(null);
                if (ingredient == null){
                    ingredient = new Ingredient(map.getKey());
                    ingredients.add(ingredient);
                }
                ingredientsRequired.put(ingredient, map.getValue());
            }
            Drink drink = new Drink(beverages.getKey(), ingredientsRequired);
            drinks.add(drink);
        }
    }

    @AfterClass
    public static void tearDown(){
        coffeeMachineImpl = null;
    }

    @Test
    public void makeDrinkTest(){

        System.out.println("\nTest Output 1\n");
        coffeeMachineImpl = new CoffeeMachineImpl(ingredients, ingredientStock, drinks);
        coffeeMachineImpl.makeDrinks(request.getMachine().getOutlets().values().iterator().next(), coffeeMachineImpl);
    }

    @Test
    public void makeDrinkTest2(){

        System.out.println("\nTest Output 2\n");
        coffeeMachineImpl = new CoffeeMachineImpl(ingredients, ingredientStock, drinks);
        coffeeMachineImpl.makeDrinks(request.getMachine().getOutlets().values().iterator().next(), coffeeMachineImpl);
    }

    @Test
    public void makeDrinkTest3(){

        System.out.println("\nTest Output 3\n");
        coffeeMachineImpl = new CoffeeMachineImpl(ingredients, ingredientStock, drinks);
        coffeeMachineImpl.makeDrinks(request.getMachine().getOutlets().values().iterator().next(), coffeeMachineImpl);
    }
}
