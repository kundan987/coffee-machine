package co.kundan.cm;


public class Ingredient {
    private final String name;

    public Ingredient(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    @Override
    public String toString() {
        return "(" + name + ")";
    }
}

