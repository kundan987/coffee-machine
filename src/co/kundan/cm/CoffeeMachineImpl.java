package co.kundan.cm;

import java.util.*;

public class CoffeeMachineImpl implements Runnable {

    private final List<Ingredient> ingredients = new ArrayList<>();
    private final Map<Ingredient, Integer> ingredientStock = new HashMap<>();
    private final List<Drink> drinks = new ArrayList<>();

    public CoffeeMachineImpl(final Collection<? extends Ingredient> ingredients, final Map<? extends Ingredient, Integer> ingredientStock, final Collection<? extends Drink> drinks) {
        this.ingredients.addAll(ingredients);
        this.ingredientStock.putAll(ingredientStock);
        this.drinks.addAll(drinks);

        this.ingredients.forEach(ingredient -> this.ingredientStock.putIfAbsent(ingredient, null));
    }

    public List<Ingredient> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    public List<Drink> getDrinks() {
        return new ArrayList<>(drinks);
    }

    public int getCurrentStock(final Ingredient ingredient) {
        checkIsValidIngredient(ingredient);
        return ingredientStock.get(ingredient);
    }

    public Map<Ingredient, Integer> getCurrentStock() {
        return ingredientStock;
    }

    public boolean isOutOfStock(final Drink drink) {
        checkIsValidDrink(drink);

        for (Map.Entry<Ingredient, Integer> ingredient : drink.getRequiredIngredients().entrySet()){
            if (ingredientStock.get(ingredient.getKey()) != null){
                if (ingredientStock.get(ingredient.getKey()) < ingredient.getValue()){
                    System.out.println(drink.getName() + " cannot be prepared because " + ingredient.getKey().getName() + " is 0");
                    return true;
                }
            } else {
                System.out.println(drink.getName() + " cannot be prepared because " + ingredient.getKey().getName() + " is not available");
                return true;
            }
        }
        return false;
    }

    public void restock(final Map<? extends Ingredient, Integer> newIngredientStock) {
        this.ingredientStock.putAll(newIngredientStock);
    }

    public void makeDrink(final Drink drink) {
        checkIsValidDrink(drink);
        if (!isOutOfStock(drink)) {
            drink.getRequiredIngredients().entrySet().stream()
                    .forEach(entry->updateStock(entry));
            System.out.println(drink.getName() + " is prepared");
        }
    }

    public void makeDrinks(int n, CoffeeMachineImpl coffeeMachine) {
        Thread[] threads = new Thread [n];

        for(int i =0; i < threads.length; i++){
            threads[i] = new Thread(coffeeMachine);
            threads[i].start();
        }
    }

    private void updateStock(Map.Entry<Ingredient, Integer> ingredient) {
        ingredientStock.compute(ingredient.getKey(), (innerIngredient, stock) -> stock - ingredient.getValue());
    }

    private void checkIsValidIngredient(final Ingredient ingredient) {
        if (!ingredients.contains(ingredient)) {
            throw new IllegalArgumentException("Unknown ingredient: " + ingredient);
        }
    }

    private void checkIsValidDrink(final Drink drink) {
        if (!drinks.contains(drink)) {
            throw new IllegalArgumentException("Unknown drink: " + drink);
        }
    }

    public static int getRandomInteger(int maximum, int minimum){
        return ((int) (Math.random()*(maximum - minimum))) + minimum;
    }

    @Override
    public void run() {
        makeDrink(drinks.get(getRandomInteger(drinks.size()-1, 0)));
    }
}
