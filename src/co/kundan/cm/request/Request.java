package co.kundan.cm.request;


import java.io.Serializable;

public class Request implements Serializable {

    private Machine machine;

    public Machine getMachine() {
        return machine;
    }

    public void setMachine(Machine machine) {
        this.machine = machine;
    }
}
