package co.kundan.cm.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Map;

public class Machine implements Serializable{

    private Map<String, Integer> outlets;

    @SerializedName("total_items_quantity")
    private Map<String, Integer> totalItemsQuantity;

    private Map<String, Map<String, Integer>> beverages;

    public Map<String, Integer> getOutlets() {
        return outlets;
    }

    public void setOutlets(Map<String, Integer> outlets) {
        this.outlets = outlets;
    }

    public Map<String, Integer> getTotalItemsQuantity() {
        return totalItemsQuantity;
    }

    public void setTotalItemsQuantity(Map<String, Integer> totalItemsQuantity) {
        this.totalItemsQuantity = totalItemsQuantity;
    }

    public Map<String, Map<String, Integer>> getBeverages() {
        return beverages;
    }

    public void setBeverages(Map<String, Map<String, Integer>> beverages) {
        this.beverages = beverages;
    }
}
