package co.kundan.cm;

import java.util.HashMap;
import java.util.Map;

public class Drink {
    private final String name;
    private final Map<Ingredient, Integer> ingredientsRequired = new HashMap<>();

    public Drink(final String name, final Map<? extends Ingredient, Integer> ingredientsRequired) {
        this.name = name;
        this.ingredientsRequired.putAll(ingredientsRequired);
    }

    public String getName() {
        return name;
    }

    public Map<Ingredient, Integer> getRequiredIngredients() {
        return ingredientsRequired;
    }

    @Override
    public String toString() {
        return "(" + name + ", " + ingredientsRequired + ")";
    }
}
